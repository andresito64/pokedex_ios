//
//  Constants.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import Foundation
struct Constants {
    struct ValueStrings {
        static let URL_BASE = "https://pokeapi.co/api/v2/pokemon"
        static let URL_IMAGE_BASE = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
        static let URL_EVOLUTION = "https://pokeapi.co/api/v2/evolution-chain/"
        static let URL_IMAGE_OFICIAL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/"
        static let STATS = "STATS"
        static let EVOLUTIONS = "EVOLUTIONS"
        static let NOT_NETWORK_CONNECTION = "The device does not have an internet connection"
        static let INTERNAL_SERVER_ERROR = "Oops it seems we had connection problems try again"
    }
    
    
    enum Connections {
        case GET_LIST_POKEMON
    }
    
    struct NatureTypes {
        static let ELECTRIC = "electric"
        static let POISON = "poison"
        static let FIRE = "fire"
        static let FLYING = "flying"
        static let GROUND = "ground"
        static let GHOST =  "ghost"
        static let PSYCHIC = "psychic"
        static let WATER = "water"
        static let DRAGON = "dragon"
        static let STEEL = "steel"
        static let FIGHT = "fighting"
        static let FAIRY = "fairy"
        static let BUG = "bug"
        static let DARK = "dark"
        static let GRASS = "grass"
        static let ICE = "ice"
        static let NORMAL = "normal"
        static let ROCK = "rock"
    }
    
    
}
