//
//  Extensions.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import UIKit
import JTProgressHUD

extension UIViewController{
    
    //push View Controller to bottom screen
    func pushBottom(_ controller: UIViewController){
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromTop
        navigationController?.view.layer.add(transition, forKey: nil)
        navigationController?.pushViewController(controller, animated: false)
    }
    
    //pop View Controller in bottom screen
    func popBottom(){
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.popViewController(animated: true)
    }
}


extension UIView{
    
    func rotate(angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = self.transform.rotated(by: radians);
        self.transform = rotation
    }

    
    var corner: CGFloat{
        get{return self.layer.cornerRadius}
        set(value){
            self.layoutIfNeeded()
            self.layer.cornerRadius = value
            self.layer.masksToBounds = true
        }
    }
    func setCustomStyle(borderColor: UIColor, borderWidth: CGFloat, cornerRadius: CGFloat) {
        self.layoutIfNeeded()
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
    
    var cornerCircular: CGFloat{
        return self.frame.height / 2
    }
    
    func addGradient(_ color1: UIColor, color2: UIColor){
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [color1.cgColor, color2.cgColor]
        self.layer.insertSublayer(gradient, at: 0)
    }
    func addGradientHorizontal(_ color1: UIColor, color2: UIColor, round: Bool = false, cornerRadius: CGFloat? = nil, frame: CGRect? = nil){
        
        let gradient = CAGradientLayer()
        gradient.frame = frame ?? self.bounds
        gradient.colors = [color1.cgColor, color2.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        if round {
            self.corner = cornerCircular
        }
        if let cornerRad = cornerRadius{
            self.corner = cornerRad
        }
        gradient.cornerRadius = self.corner
        self.layer.insertSublayer(gradient, at: 0)
        self.layoutIfNeeded()
        self.layer.masksToBounds = true
    }
    
    func addBottomConstraintParent(constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.bottomAnchor.constraint(equalTo: self.superview!.bottomAnchor, constant: constant).isActive = true
    }
    
    func addWidthConstraintParent(multiplier: CGFloat = 1){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalTo: self.superview!.widthAnchor, multiplier: multiplier).isActive = true
    }
    
    func addWidthConstraint(_ view: UIView,multiplier: CGFloat = 1){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: multiplier).isActive = true
    }
    
    func addHeightConstraintParent(multiplier: CGFloat = 1){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalTo: self.superview!.heightAnchor, multiplier: multiplier).isActive = true
    }
    
    func addLeadingContraintParent(constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: self.superview!.leadingAnchor, constant: constant).isActive = true
    }
    
    func addLeadingContraint(_ view: UIView, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: constant).isActive = true
    }
    
    func addRightContraint(_ view: UIView, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.trailingAnchor.constraint(equalTo: view.leadingAnchor, constant: constant).isActive = true
    }
    
    func addTrailingConstraintParent(constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.trailingAnchor.constraint(equalTo: self.superview!.trailingAnchor, constant: constant).isActive = true
    }
    
    func addTrailingConstraint(_ view: UIView, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: constant).isActive = true
    }
    
    func addLeftConstraint(_ view: UIView, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: constant).isActive = true
    }
    
    func addCenterYConstraint(_ view: UIView, multiplier: CGFloat = 1, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .centerY,
                                            relatedBy: .equal,
                                            toItem: view,
                                            attribute: .centerY,
                                            multiplier: multiplier,
                                            constant: constant)
        constraint.isActive = true
        self.superview!.addConstraint(constraint)
    }
    
    func addCenterXConstraint(_ view: UIView, multiplier: CGFloat = 1, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .centerX,
                                            relatedBy: .equal,
                                            toItem: view,
                                            attribute: .centerX,
                                            multiplier: multiplier,
                                            constant: constant)
        constraint.isActive = true
        self.superview!.addConstraint(constraint)
    }
    
    func addCenterTopConstraints(_ view: UIView, constant: CGFloat = 0){
        self.addConstraintTop(view, constant: constant)
        self.addConstraintCenterXParent()
    }
    func addConstraintTopParent( constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: self.superview!.topAnchor, constant: constant).isActive = true
    }
    
    func addConstraintBottomParent( constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.bottomAnchor.constraint(equalTo: self.superview!.bottomAnchor, constant: constant).isActive = true
    }
    
    func addConstraintYCenterParent(multiplier: CGFloat = 1, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .centerY,
                                            relatedBy: .equal,
                                            toItem: self.superview!,
                                            attribute: .centerYWithinMargins,
                                            multiplier: multiplier,
                                            constant: constant)
        constraint.isActive = true
        self.superview!.addConstraint(constraint)
    }
    
    func addConstraintXCenterParent(multiplier: CGFloat = 1, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .centerX,
                                            relatedBy: .equal,
                                            toItem: self.superview!,
                                            attribute: .centerX,
                                            multiplier: multiplier,
                                            constant: constant)
        constraint.isActive = true
        self.superview!.addConstraint(constraint)
    }
    
    func addConstraintTop(_ view: UIView, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: view.bottomAnchor, constant: constant).isActive = true
    }
    
    func addConstraintBottom(_ view: UIView, constant: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.bottomAnchor.constraint(equalTo: view.topAnchor, constant: constant).isActive = true
    }
    
    func addConstraintCenterXParent( width: CGFloat? = nil, height: CGFloat? = nil){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.centerXAnchor.constraint(equalTo: self.superview!.centerXAnchor).isActive = true
        if let width = width {self.setWidthConstraint(width)}
        if let height = height {self.setHeightConstraint(height)}
    }
    
    
    func setWidthConstraint(_ width: CGFloat){
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraint = self.widthAnchor.constraint(equalToConstant: width)
        constraint.identifier = "widthConstraint"
        constraint.isActive = true
    }
    
    func setHeightConstraint(_ height: CGFloat){
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraint = self.heightAnchor.constraint(equalToConstant: height)
        constraint.identifier = "hightConstraint"
        constraint.isActive = true
    }
    
    func setSize(width: CGFloat? = nil, height: CGFloat? = nil){
        if let width = width {self.setWidthConstraint(width)}
        if let height = height {self.setHeightConstraint(height)}
    }
    
    func resize(height: CGFloat, width: CGFloat? = nil){
        for constraint in self.constraints where constraint.identifier == "hightConstraint"{
            constraint.isActive = false
            constraint.identifier = ""
        }
        self.setSize(height: height)
        if let width = width{
            for constraint in self.constraints where constraint.identifier == "widthConstraint"{
                constraint.isActive = false
                constraint.identifier = ""
            }
            self.setSize(width: width)
        }
    }
    
    func addGesture(_ viewController: UIViewController, selector: Selector){
        self.addGestureRecognizer(UITapGestureRecognizer(target: viewController, action: selector))
    }
    
    func addSubviewWithConstraint(_ child: UIView,
                                  topParent: Bool = false,
                                  constantTop: CGFloat? = nil,
                                  bottomParent: Bool = false,
                                  constantBottom: CGFloat? = nil,
                                  widhtParent: Bool = false,
                                  heightParent: Bool = false,
                                  widthAnchor: CGFloat? = nil,
                                  heightAnchor: CGFloat? = nil,
                                  centerX: Bool = false,
                                  centerY: Bool = false,
                                  leadingParent: Bool = false,
                                  trailingParent: Bool = false,
                                  constantLeading: CGFloat? = nil,
                                  constantTrailing: CGFloat? = nil,
                                  width: CGFloat? = nil,
                                  height: CGFloat? = nil){
        self.addSubview(child)
        if topParent {
            child.addConstraintTopParent(constant: constantTop ?? 0)
        }
        if bottomParent {
            child.addConstraintBottomParent(constant: constantBottom ?? 0)
        }
        if widhtParent {
            child.addWidthConstraintParent(multiplier: widthAnchor ?? 1)
        }
        if heightParent {
            child.addHeightConstraintParent(multiplier: heightAnchor ?? 1)
        }
        if centerX {
            child.addConstraintCenterXParent()
        }
        if centerY {
            child.addConstraintYCenterParent()
        }
        if leadingParent {
            child.addLeadingContraintParent(constant: constantLeading ?? 0)
        }
        if trailingParent{
            child.addTrailingConstraintParent(constant: constantTrailing ?? 0)
        }
        if width != nil{
            child.setSize(width: width)
        }
        if let height = height {
            child.setSize(height: height)
        }
        
    }
    
    func removeAllViews(){
        for view in self.subviews{
            view.removeFromSuperview()
        }
    }
    
    func setShadowColor(_ color: UIColor, cornerRadius: CGFloat){
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: -1, height: -1)
        layer.shadowRadius = cornerRadius
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
    
    func calculateHeight() -> CGFloat{
        var height: CGFloat = 0
        for subview in self.subviews{
            if !subview.isHidden {
                height += subview.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            }
        }
        return height
    }
}


extension UILabel{
    func setParameters(text: String? = nil,
                       color: UIColor? = nil,
                       font: UIFont? = nil,
                       lines: Int? = nil){
        if let text = text {self.text = text}
        if let color = color {self.textColor = color}
        if let font = font {self.font = font}
        if let lines = lines{self.numberOfLines = lines}
    }
    
    func height(width: CGFloat) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font as Any], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return CGFloat(linesRoundedUp) * self.font.lineHeight
    }
}

//start Progress Waiting
func showProgress(){
    DispatchQueue.main.async {
        JTProgressHUD.show()
    }
}

//end Progress Waiting
func closeProgress(){
    DispatchQueue.main.async {
        JTProgressHUD.hide()
    }
}



extension Data{
    //Convert Object Data to object Decodable
    func getObject<T: Codable>(_ type: T.Type) -> Codable?{
        do {
            let object  = try JSONDecoder().decode(T.self, from: self)
            return object
        } catch  {
            print(error.localizedDescription)
            return nil
        }
    }
    
    //Comvert Object data to Array Object decodable
    func getArrayObject<T: Codable>(_ type: T.Type) -> [Codable]?{
        do {
            let object  = try JSONDecoder().decode(Array<T>.self, from: self)
            return object
        } catch  {
            print(error)
            return nil
        }
    }
}


//Extension colors used on System
extension UIColor{
    static let bug1 = UIColor(hex: "92bc2c")
    static let bug2 = UIColor(hex: "aec836")
    static let dark1 = UIColor(hex: "595661")
    static let dark2 = UIColor(hex: "6c7486")
    static let dragon1 = UIColor(hex: "0e69c8")
    static let dragon2 = UIColor(hex: "0280c7")
    static let fire1 = UIColor(hex: "fb9b50")
    static let fire2 = UIColor(hex: "fbad46")
    static let flying1 = UIColor(hex: "90a7da")
    static let flying2 = UIColor(hex: "a5c1f1")
    static let ghost1 = UIColor(hex: "526aac")
    static let ghost2 = UIColor(hex: "7673d3")
    static let water1 = UIColor(hex: "7673d3")
    static let water2 = UIColor(hex: "69b9e3")
    static let electric1 = UIColor(hex: "edd53e")
    static let electric2 = UIColor(hex: "fae272")
    static let fairy1 = UIColor(hex: "ec8ce5")
    static let fairy2 = UIColor(hex: "f3a6e7")
    static let fight1 = UIColor(hex: "ce4264")
    static let fight2 = UIColor(hex: "e64348")
    static let grass1 = UIColor(hex: "5fbc51")
    static let grass2 = UIColor(hex: "5ac177")
    static let ground1 = UIColor(hex: "dc7645")
    static let ground2 = UIColor(hex: "d29362")
    static let ice1 = UIColor(hex: "70ccbd")
    static let ice2 = UIColor(hex: "8bddd3")
    static let normal1 = UIColor(hex: "9298a4")
    static let normal2 = UIColor(hex: "a3a49e")
    static let poison1 = UIColor(hex: "a864c7")
    static let poison2 = UIColor(hex: "c161d3")
    static let steel1 = UIColor(hex: "52869d")
    static let steel2 = UIColor(hex: "59a5a9")
    static let psychic1 = UIColor(hex: "f66f71")
    static let psychic2 = UIColor(hex: "fd9d91")
    static let rock1 = UIColor(hex: "c5b489")
    static let rock2 = UIColor(hex: "d6cc90")
    
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }
    
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = String(hex.prefix(1))
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt64 = 0x0
        scanner.scanHexInt64(&hexInt)
        
        var r:UInt64!, g:UInt64!, b:UInt64!
        switch (hexWithoutSymbol.count) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
}
