//
//  Connection.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import Alamofire

class Connection {
    init(method: Alamofire.HTTPMethod) {
        self.method = method
    }
    let method: Alamofire.HTTPMethod
}


extension Encodable {
    //function convert object encodable/decodable to dictionary for parse
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
    
    //function convert object encodable/decodable to Json Object
    func toJson() -> String? {
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dictionary!,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            return theJSONText
        }
        else {return nil}
    }
}

extension Constants.Connections{
    
    //return method hhtp used
    var method:Alamofire.HTTPMethod{
        get{
            switch self {
            case .GET_LIST_POKEMON:
                return .get
            }
        }
    }

    //validate if network is available
    func isNetworkAvaible() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }


    //function with alamofire to call server informacion.
    func requestService(_
                            body: Codable? = nil,
                            url: String? = nil,
                            listener: @escaping (_ Object: Data?, _ error:String?) -> Void,
                            isProgress: Bool = true) {
        DispatchQueue.global().async {
            if !self.isNetworkAvaible() {
                listener(nil, Constants.ValueStrings.NOT_NETWORK_CONNECTION)
            }
            var url: String = url ?? Constants.ValueStrings.URL_BASE
            if self.method == .get{
                url += "?"
                body?.dictionary!.forEach{
                    body in
                    url  += "\(body.key)=\(body.value)&"
                }
                url = url.replacingOccurrences(of: " ", with: "")
            }
            if isProgress{showProgress()}
            Alamofire.request(url,
                              method: self.method,
                              parameters: (self.method != .get) ? body?.dictionary : nil,
                              encoding: JSONEncoding.default,
                              headers: nil).responseData{ response in
                                if isProgress {closeProgress()}
                                if let error  = response.result.error?.localizedDescription{
                                    listener(nil, error)
                                    return
                                }
                                if response.response?.statusCode != 200{
                                    listener(nil, Constants.ValueStrings.INTERNAL_SERVER_ERROR)
                                    return
                                }
                                return listener(response.result.value, nil)
            }
        }
    }

}
