//
//  Request.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import Foundation

//Model of request initial listPreview Pokemon
struct RequestListPokemon: Codable {
    let offset: String
    var limit: String = "1118"
}
