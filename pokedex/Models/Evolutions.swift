//
//  Evolutions.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import Foundation

// MARK: - Evolution
struct Evolution: Codable {
    var chain: Chain
    var id: Int

    enum CodingKeys: String, CodingKey {
        case chain, id
    }
}

// MARK: - Chain
struct Chain: Codable {
    var evolutionDetails: [EvolutionDetail]
    var evolvesTo: [Chain]
    var isBaby: Bool
    var species: SpeciesModel

    enum CodingKeys: String, CodingKey {
        case evolutionDetails = "evolution_details"
        case evolvesTo = "evolves_to"
        case isBaby = "is_baby"
        case species
    }
    
    
    //generate url image of pokemon
    func getPhoto() -> String{
        return Constants.ValueStrings.URL_IMAGE_OFICIAL + species.url!.split(separator: "/").last! + ".png"
    }
}

// MARK: - EvolutionDetail
struct EvolutionDetail: Codable {
    var minLevel: Int?
    var item: SpeciesModel?
    var needsOverworldRain: Bool?
    var timeOfDay: String?
    var trigger: Species?
    var turnUpsideDown: Bool?

    enum CodingKeys: String, CodingKey {

        case minLevel = "min_level"
        case needsOverworldRain = "needs_overworld_rain"
        case timeOfDay = "time_of_day"
        case trigger
        case turnUpsideDown = "turn_upside_down"
        case item
    }
}
