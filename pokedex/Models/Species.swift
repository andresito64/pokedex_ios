//
//  Species.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import Foundation

// MARK: - Species
struct Species: Codable {
    var baseHappiness, captureRate: Int?
    var color: Color?
    var eggGroups: [Color]?
    var evolutionChain: EvolutionChain?
    var flavorTextEntries: [FlavorTextEntry]?
    var formsSwitchable: Bool?
    var genderRate: Int?
    var genera: [Genus]?
    var generation, growthRate, habitat: Color?
    var hasGenderDifferences: Bool?
    var hatchCounter, id: Int?
    var isBaby, isLegendary, isMythical: Bool?
    var name: String?
    var names: [Name]?
    var order: Int?
    var palParkEncounters: [PalParkEncounter]?
    var pokedexNumbers: [PokedexNumber]?
    var shape: Color?
    var varieties: [Variety]?

    enum CodingKeys: String, CodingKey {
        case baseHappiness = "base_happiness"
        case captureRate = "capture_rate"
        case color
        case eggGroups = "egg_groups"
        case evolutionChain = "evolution_chain"
        case flavorTextEntries = "flavor_text_entries"
        case formsSwitchable = "forms_switchable"
        case genderRate = "gender_rate"
        case genera, generation
        case growthRate = "growth_rate"
        case habitat
        case hasGenderDifferences = "has_gender_differences"
        case hatchCounter = "hatch_counter"
        case id
        case isBaby = "is_baby"
        case isLegendary = "is_legendary"
        case isMythical = "is_mythical"
        case name, names, order
        case palParkEncounters = "pal_park_encounters"
        case pokedexNumbers = "pokedex_numbers"
        case shape, varieties
    }
}

// MARK: - Color
struct Color: Codable {
    var name: String
    var url: String
}

// MARK: - EvolutionChain
struct EvolutionChain: Codable {
    var url: String
}

// MARK: - FlavorTextEntry
struct FlavorTextEntry: Codable {
    var flavorText: String
    var language, version: Color

    enum CodingKeys: String, CodingKey {
        case flavorText = "flavor_text"
        case language, version
    }
}

// MARK: - Genus
struct Genus: Codable {
    var genus: String
    var language: Color
}

// MARK: - Name
struct Name: Codable {
    var language: Color
    var name: String
}

// MARK: - PalParkEncounter
struct PalParkEncounter: Codable {
    var area: Color
    var baseScore, rate: Int

    enum CodingKeys: String, CodingKey {
        case area
        case baseScore = "base_score"
        case rate
    }
}

// MARK: - PokedexNumber
struct PokedexNumber: Codable {
    var entryNumber: Int
    var pokedex: Color

    enum CodingKeys: String, CodingKey {
        case entryNumber = "entry_number"
        case pokedex
    }
}

// MARK: - Variety
struct Variety: Codable {
    var isDefault: Bool
    var pokemon: Color

    enum CodingKeys: String, CodingKey {
        case isDefault = "is_default"
        case pokemon
    }
}
