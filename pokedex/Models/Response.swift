//
//  Response.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import Foundation
struct ResponseList: Codable {
    let count: Int
    let next: String?
    let previous: String?
    let results: [PokemonPreview]?
}
