//
//  PokemonPreview.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import Foundation
struct PokemonPreview: Codable {
    let name: String
    let url: String
    var position: Int? = nil
    var pokemon: Pokemon? = nil
    
    //generate url imagePreview of pokemon
    func getUrlPhoto() -> String{
        return Constants.ValueStrings.URL_IMAGE_BASE + position!.description + ".png"
    }
    
    
    //generate number of pokemon
    func getNumber() -> String{
        if position == nil {return "#000"}
        if position! < 10{ return "#00\(position!)"}
        if position! > 9 && position! < 100{ return "#0\(position!)"}
        else {return "#\(position!)"}
    }
}
