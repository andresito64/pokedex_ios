//
//  PokemonDetailViewDelegate.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import Foundation

//Delegate for communication between the viewer and the presenter
protocol PokemonDetailViewDelegate {
    func setDescription(_ specie: Species)
    func setEvolution(_ evolution: Evolution)
}
