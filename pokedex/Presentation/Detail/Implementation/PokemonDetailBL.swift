//
//  PokemonDetailBl.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import Foundation
class PokemonDetailBL {
    
    
    //called service to get specie of pokemon
    func getSpecie(_ response: @escaping((Species) -> Void), url: String){
        
        Constants.Connections.GET_LIST_POKEMON.requestService( url: url,
                                                               listener: { (data, message) in
                                                                if let especie = data?.getObject(Species.self) as? Species{
                                                                    response(especie)
                                                                }
                                                               },
                                                               isProgress: false)
    }
    
    
    //called service to get evolutions of pokemon
    func getEvolution(_ response: @escaping((Evolution) -> Void), url: String){
        Constants.Connections.GET_LIST_POKEMON.requestService(url: url,
                                                              listener: { (data, message) in
                                                                if let evolution = data?.getObject(Evolution.self) as? Evolution{
                                                                    response(evolution)
                                                                }
                                                              },
                                                              isProgress: false)
    }
}
