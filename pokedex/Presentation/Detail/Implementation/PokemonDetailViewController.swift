//
//  PokemonDetailViewController.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import UIKit

class PokemonDetailViewController: BaseViewController {
    
    var presenter: PokemonDetailPresenter!
    var pokemon: Pokemon!
    var color1: UIColor!
    var color2: UIColor!
    var size: CGFloat!
    var information: UILabel!
    var viewStats: UIView!
    var viewEvolution: UIView!
    var contentView: UIView!
    var scrollView: UIScrollView!
    var viewMoves: UIView!
    var buttonStats: UIButton!
    var buttonEvolutions: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = PokemonDetailPresenter(self)
        setView()
        presenter.getInformation(pokemon.species.url!)
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    //this functions recalculate to height and with size of screen but resize to scroll content
    func recalculeHeight(){
        contentView.layoutIfNeeded()
        contentView.resize(height: contentView.calculateHeight() * 1.4)
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: contentView.calculateHeight() * 1.2 + 150)
    }
    

    //this function generate all views in screen
    private func setView(){
        scrollView = UIScrollView()
        size = (self.view.frame.width > self.view.frame.height) ? self.view.frame.width : self.view.frame.height
        self.view.addSubviewWithConstraint(scrollView, topParent: true, widhtParent: true, heightParent: true, centerX: true)
        setColors()
        contentView = UIView()
        scrollView.addSubviewWithConstraint(contentView,topParent: true, constantTop: 200, widhtParent: true, centerX: true, height: size * 1.2)
        setViewContent(contentView)
        if let photo = pokemon.sprites?.other?.officialArtwork.frontDefault{
            let imagePokemon = UIImageView()
            imagePokemon.contentMode = .scaleAspectFit
            scrollView.addSubviewWithConstraint(imagePokemon, topParent: true, constantTop: 80,centerX: true, height: 160)
            imagePokemon.kf.setImage(with: URL(string: photo))
        }
        let imageDown = UIImageView(image: #imageLiteral(resourceName: "icon_down"))
        scrollView.addSubviewWithConstraint(imageDown, topParent: true, constantTop: 30, leadingParent: true, constantLeading: 10, width: 30, height: 30)
        imageDown.contentMode = .scaleAspectFit
        imageDown.isUserInteractionEnabled = true
        imageDown.addGesture(self, selector: #selector(onClickDown))
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: size + 150)
        
    }
    
    //set general view
    func setViewContent(_ content: UIView){
        content.backgroundColor = .white
        content.corner = 20
        let name = UILabel()
        content.addSubviewWithConstraint(name, topParent: true, constantTop: 40, centerX: true)
        name.setParameters(text: pokemon.name.capitalized, color: .black, font: UIFont.boldSystemFont(ofSize: 22), lines: 1)
        if pokemon.types.count == 1 {
            let buttonImage = createButton(pokemon.types.first!.type!.name!)
            content.addSubviewWithConstraint(buttonImage, centerX: true)
            buttonImage.addConstraintTop(name, constant: 20)
        }
        else if pokemon.types.count > 1{
            let buttonImage = createButton(pokemon.types.first!.type!.name!)
            content.addSubview(buttonImage)
            buttonImage.addConstraintXCenterParent(multiplier: 0.65)
            buttonImage.addConstraintTop(name, constant: 20)
            
            let buttonImage2 = createButton(pokemon.types.last!.type!.name!)
            content.addSubview(buttonImage2)
            buttonImage2.addConstraintXCenterParent(multiplier: 1.35)
            buttonImage2.addConstraintTop(name, constant: 20)
        }
        information = UILabel()
        content.addSubviewWithConstraint(information, widhtParent: true, widthAnchor: 0.9, centerX: true)
        information.textAlignment = .center
        information.addConstraintTop(name, constant: 70)
        
        buttonStats = UIButton(type: .custom)
        content.addSubviewWithConstraint(buttonStats, widhtParent: true, widthAnchor: 0.3, height: 40)
        buttonStats.addConstraintTop(information, constant: 20)
        buttonStats.addConstraintXCenterParent(multiplier: 0.5)
        buttonStats.backgroundColor = color1
        buttonStats.corner =  20
        buttonStats.setTitleColor(.white, for: .normal)
        buttonStats.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        buttonStats.setTitle(Constants.ValueStrings.STATS, for: .normal)
        buttonStats.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        
        viewStats = UIView()
        content.addSubviewWithConstraint(viewStats, widhtParent: true, centerX: true)
        viewStats.addConstraintTop(buttonStats)
        setStats(parent: viewStats)
        
        
        buttonEvolutions = UIButton(type: .custom)
        content.addSubviewWithConstraint(buttonEvolutions, widhtParent: true, widthAnchor: 0.3,  height: 40)
        buttonEvolutions.addConstraintTop(information, constant: 20)
        buttonEvolutions.addConstraintXCenterParent(multiplier: 1.5)
        buttonEvolutions.corner =  20
        buttonEvolutions.setTitleColor(color1, for: .normal)
        buttonEvolutions.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        buttonEvolutions.setTitle(Constants.ValueStrings.EVOLUTIONS, for: .normal)
        buttonEvolutions.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        
        viewEvolution = UIView()
        content.addSubviewWithConstraint(viewEvolution, widhtParent: true, centerX: true)
        viewEvolution.addConstraintTop(buttonStats)
        viewEvolution.isHidden = true
        
        viewMoves = UIView()
        content.addSubviewWithConstraint(viewMoves, widhtParent: true, centerX: true)
        viewMoves.addConstraintTop(buttonStats)
        viewMoves.isHidden = true
    }
    
    // Set views Stats of pokemon
    func setStats(parent: UIView){
        var lastView: UIView!
        for stat in pokemon.stats{
            let viewStat = UIView()
            parent.addSubviewWithConstraint(viewStat, widhtParent: true,centerX: true, height: 20)
            if lastView == nil{
                viewStat.addConstraintTopParent(constant: 10)
            }
            else{
                viewStat.addConstraintTop(lastView, constant: 10)
            }
            let name = UILabel()
            viewStat.addSubviewWithConstraint(name, centerY: true, leadingParent: true, constantLeading: 10,  width: 70)
            name.adjustsFontSizeToFitWidth = true
            name.minimumScaleFactor = 0.3
            name.setParameters(text: stat.stat?.name?.uppercased(), color: color1, font: UIFont.systemFont(ofSize: 12), lines: 1)
            let value = UILabel()
            viewStat.addSubviewWithConstraint(value, centerY: true, width: 25)
            value.addLeftConstraint(name, constant: 5)
            value.textAlignment = .right
            value.setParameters(text: stat.baseStat!.description, color: .black, font: UIFont.systemFont(ofSize: 12), lines: 1)
            let progress = UIProgressView()
            viewStat.addSubviewWithConstraint(progress, centerY: true, trailingParent: true, constantTrailing: -20)
            progress.addLeftConstraint(value, constant: 10)
            progress.layoutIfNeeded()
            progress.progressTintColor = color1
            progress.setProgress(Float(stat.baseStat!) * 0.01, animated: true)
            lastView = viewStat
        }
        let viewBottom = UIView()
        parent.addSubviewWithConstraint(viewBottom, widhtParent: true, centerX: true, height: 100)
        viewBottom.addConstraintTop(lastView)
        parent.layoutIfNeeded()
        parent.setSize(height: parent.calculateHeight())
    }
    
    //Trigger when the screen rotates to resize the scrollView
    @objc func rotated() {
        recalculeHeight()
    }
    
    //click action to buttons buttonStart and buttonEvolutions
    @objc func onClickButton(_ button: UIButton){
        switch button {
        case buttonStats:
            buttonEvolutions.backgroundColor = .clear
            buttonEvolutions.setTitleColor(color1, for: .normal)
            viewStats.isHidden = false
            viewEvolution.isHidden = true
            viewMoves.isHidden = true
            break
        case buttonEvolutions:
            buttonStats.backgroundColor = .clear
            buttonStats.setTitleColor(color1, for: .normal)
            viewStats.isHidden = true
            viewEvolution.isHidden = false
            viewMoves.isHidden = true
            break
        default:
            break
        }
        button.backgroundColor = color1
        button.setTitleColor(.white, for: .normal)
        recalculeHeight()
    }
    
    
    //This function generates the nature buttons of the Pokemon
    private func createButton(_ name: String) -> UIImageView{
        let imageNature = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width * 0.3, height: 30))
        imageNature.corner = imageNature.cornerCircular
        imageNature.contentMode = .scaleToFill
        switch name {
        case Constants.NatureTypes.BUG:
            imageNature.image = #imageLiteral(resourceName: "Bug-1")
            break
        case Constants.NatureTypes.DARK:
            imageNature.image = #imageLiteral(resourceName: "Dark-1")
            break
        case Constants.NatureTypes.DRAGON:
            imageNature.image = #imageLiteral(resourceName: "Dragon-1")
            break
        case Constants.NatureTypes.ELECTRIC:
            imageNature.image = #imageLiteral(resourceName: "Electric-1")
            break
        case Constants.NatureTypes.FAIRY:
            imageNature.image = #imageLiteral(resourceName: "Fairy-1")
            break
        case Constants.NatureTypes.FIGHT:
            imageNature.image = #imageLiteral(resourceName: "Fight-1")
            break
        case Constants.NatureTypes.FIRE:
            imageNature.image = #imageLiteral(resourceName: "Fire-1")
            break
        case Constants.NatureTypes.FLYING:
            imageNature.image = #imageLiteral(resourceName: "Flying-1")
            break
        case Constants.NatureTypes.GHOST:
            imageNature.image = #imageLiteral(resourceName: "Ghost-1")
            break
        case Constants.NatureTypes.GRASS:
            imageNature.image = #imageLiteral(resourceName: "Grass-1")
            break
        case Constants.NatureTypes.GROUND:
            imageNature.image = #imageLiteral(resourceName: "Ground-1")
            break
        case Constants.NatureTypes.ICE:
            imageNature.image = #imageLiteral(resourceName: "Ice-1")
            break
        case Constants.NatureTypes.NORMAL:
            imageNature.image = #imageLiteral(resourceName: "Normal-1")
            break
        case Constants.NatureTypes.POISON:
            imageNature.image = #imageLiteral(resourceName: "Poison-1")
            break
        case Constants.NatureTypes.PSYCHIC:
            imageNature.image = #imageLiteral(resourceName: "Psychic-1")
            break
        case Constants.NatureTypes.ROCK:
            imageNature.image = #imageLiteral(resourceName: "Rock-1")
            break
        case Constants.NatureTypes.STEEL:
            imageNature.image = #imageLiteral(resourceName: "Steel-1")
            break
        case Constants.NatureTypes.WATER:
            imageNature.image = #imageLiteral(resourceName: "Water-1")
            break
        default:
            break
        }
        
        return imageNature
    }
    
    
    //This function selected two colors base to gradient screen
    private func setColors(){
        switch pokemon.types.first?.type?.name {
        case Constants.NatureTypes.BUG:
            color1 = .bug1
            color2 = .bug2
            break
        case Constants.NatureTypes.DARK:
            color1 = .dark1
            color2 = .dark2
            break
        case Constants.NatureTypes.DRAGON:
            color1 = .dragon1
            color2 = .dragon2
            break
        case Constants.NatureTypes.ELECTRIC:
            color1 = .electric1
            color2 = .electric2
            break
        case Constants.NatureTypes.FAIRY:
            color1 = .fairy1
            color2 = .fairy2
            break
        case Constants.NatureTypes.FIGHT:
            color1 = .fight1
            color2 = .fight2
            break
        case Constants.NatureTypes.FIRE:
            color1 = .fire1
            color2 = .fire2
            break
        case Constants.NatureTypes.FLYING:
            color1 = .flying1
            color2 = .flying2
            break
        case Constants.NatureTypes.GHOST:
            color1 = .ghost1
            color2 = .ghost2
            break
        case Constants.NatureTypes.GRASS:
            color1 = .grass1
            color2 = .grass2
            break
        case Constants.NatureTypes.GROUND:
            color1 = .ground1
            color2 = .ground2
            break
        case Constants.NatureTypes.ICE:
            color1 = .ice1
            color2 = .ice2
            break
        case Constants.NatureTypes.NORMAL:
            color1 = .normal1
            color2 = .normal2
            break
        case Constants.NatureTypes.POISON:
            color1 = .poison1
            color2 = .poison2
            break
        case Constants.NatureTypes.PSYCHIC:
            color1 = .psychic1
            color2 = .psychic2
            break
        case Constants.NatureTypes.ROCK:
            color1 = .rock1
            color2 = .rock2
            break
        case Constants.NatureTypes.STEEL:
            color1 = .steel1
            color2 = .steel2
            break
        case Constants.NatureTypes.WATER:
            color1 = .water1
            color2 = .water2
            break
        
        default:
            break
        }
        self.view.addGradientHorizontal(color1, color2: color2, frame: CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: size, height: size))
    }
    
    
    //Click action to exit viewController
    @objc func onClickDown(){
        popBottom()
    }

}
