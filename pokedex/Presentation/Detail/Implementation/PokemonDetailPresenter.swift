//
//  PokemonDetailPresenter.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import Foundation
class PokemonDetailPresenter{
    
    var pokemonBL: PokemonDetailBL!
    var pokemonDetail: PokemonDetailViewDelegate!
    
    init(_ pokemonDetail: PokemonDetailViewDelegate) {
        self.pokemonDetail = pokemonDetail
        self.pokemonBL = PokemonDetailBL()
    }
    
    
    
    //Get information Spice to pokemon with url
    func getInformation(_ url: String){
        pokemonBL.getSpecie({ (spicie) in
            DispatchQueue.main.async {
                self.pokemonDetail.setDescription(spicie)
            }
            
        }, url: url)
    }
    
    //Get information to evolutions of pokemon
    func getEvolution(_ url: String){
        pokemonBL.getEvolution({ (evolution) in
            DispatchQueue.main.async {
                self.pokemonDetail.setEvolution(evolution)
            }
        }, url: url)
    }
    
}
