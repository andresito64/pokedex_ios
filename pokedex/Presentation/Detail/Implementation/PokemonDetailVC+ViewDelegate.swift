//
//  PokemonDetailVC+ViewDelegate.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import UIKit
extension PokemonDetailViewController: PokemonDetailViewDelegate{
    
    //function number 1 to create views of evolution
    func setEvolution(_ evolution: Evolution) {
        for envol in evolution.chain.evolvesTo {
            addEvolution(envol, name: evolution.chain.species.name!, photo: evolution.chain.getPhoto(), lasView: self.viewEvolution.subviews.last)
        }
        self.viewEvolution.layoutIfNeeded()
        self.viewEvolution.setSize(height: self.viewEvolution.calculateHeight())
        self.recalculeHeight()
    }
    
    //function number 2 to create views of evolution this function is cyclic
    func addEvolution(_ envol: Chain, name: String, photo: String, lasView: UIView? = nil){
        let viewEvo = UIView()
        self.viewEvolution.addSubviewWithConstraint(viewEvo, widhtParent: true, centerX: true, height: 120)
        if lasView == nil {
            viewEvo.addConstraintTopParent()
        }
        else{
            viewEvo.addConstraintTop(lasView!)
        }
        let image1 = UIImageView()
        viewEvo.addSubviewWithConstraint(image1, centerY: true, leadingParent: true, constantLeading: 20,  width: 80,height: 80)
        image1.contentMode = .scaleAspectFit
        image1.kf.setImage(with: URL(string: photo))
        let name1 = UILabel()
        viewEvo.addSubview(name1)
        name1.addConstraintTop(image1, constant: 5)
        name1.addCenterXConstraint(image1)
        name1.setParameters(text: name.capitalized, color: .black, font: UIFont.systemFont(ofSize: 10), lines: 0)
        
        let image2 = UIImageView()
        viewEvo.addSubviewWithConstraint(image2, centerY: true, trailingParent: true, constantTrailing: -20,  width: 80, height: 80)
        image2.contentMode = .scaleAspectFit
        image2.kf.setImage(with: URL(string: envol.getPhoto()))
        
        let name2 = UILabel()
        viewEvo.addSubview(name2)
        name2.addConstraintTop(image2, constant: 5)
        name2.addCenterXConstraint(image2)
        name2.setParameters(text: envol.species.name?.capitalized, color: .black, font: UIFont.systemFont(ofSize: 10), lines: 0)
        let lineMid = UIView()
        viewEvo.addSubviewWithConstraint(lineMid, centerY: true, height: 1)
        lineMid.addLeftConstraint(image1, constant: 10)
        lineMid.addRightContraint(image2, constant: -10)
        lineMid.backgroundColor = .lightGray
        
        let labeLevel = UILabel()
        viewEvo.addSubview(labeLevel)
        var level = ""
        if let item = envol.evolutionDetails.first?.item{
            level = item.name!.capitalized
        }
        else if let levelMin = envol.evolutionDetails.first?.minLevel{
            level = "Lv. \(levelMin)"
        }
        labeLevel.setParameters(text: level, color: color1, font: UIFont.systemFont(ofSize: 12), lines: 1)
        labeLevel.addConstraintBottom(lineMid, constant: -5)
        labeLevel.addCenterXConstraint(lineMid)
        let line = UIView()
        viewEvo.addSubviewWithConstraint(line, bottomParent: true, widhtParent: true, widthAnchor: 0.9, centerX: true, height: 1)
        line.backgroundColor = .lightGray
        for envol2 in envol.evolvesTo{
            addEvolution(envol2, name: envol.species.name!, photo: envol.getPhoto(), lasView: self.viewEvolution.subviews.last)
        }
    }
    
    
    // this function set information of pokemon and call information to evolutions
    func setDescription(_ specie: Species) {
        if let desc = specie.flavorTextEntries?.first(where: { (flavor) -> Bool in
            flavor.language.name == "en"
        })?.flavorText{
            information.setParameters(text: desc.replacingOccurrences(of: "\n", with: "").capitalized, color: .black, font: UIFont.systemFont(ofSize: 12), lines: 0)
        }
        if let urlEvolution = specie.evolutionChain?.url{
            self.presenter.getEvolution(urlEvolution)
        }
    }
    
    
    
}
