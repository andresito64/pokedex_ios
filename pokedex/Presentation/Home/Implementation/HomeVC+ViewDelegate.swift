//
//  HomeVC+ViewDelegate.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import UIKit

extension HomeViewController: HomeViewDelegate{
    
    
    // only reload data information of pokemons
    func reloadTable() {
        self.tablePokemon.reloadData()
    }
    
    //Only if the user is not scrolling then the information obtained from the server is reloaded
    func reloadTableValidate() {
        if(!self.isScroll){
            if self.search.text?.isEmpty == false {
                self.presenter.evaluateFilter(self.search.text!)
            }
            else{
                self.tablePokemon.reloadData()
            }
        }
    }
    
}
