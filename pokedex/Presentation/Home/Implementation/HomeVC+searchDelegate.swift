//
//  HomeVC+searchDelegate.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import UIKit

//delegate called interactions with searchBar
extension HomeViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter.evaluateFilter(searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text?.isEmpty == false {
            self.presenter.evaluateFilter(searchBar.text!)
        }
    }
    
}



