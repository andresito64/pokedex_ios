//
//  HomeBL.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import Foundation
class HomeBL {
    
    //Called Service to get list preview pokemon
    func getListPokemon(_ response:  @escaping( (ResponseList) -> Void)){
        Constants.Connections.GET_LIST_POKEMON.requestService(RequestListPokemon(offset: "0")) { (data, message) in
            if let dataServer = data?.getObject(ResponseList.self) as? ResponseList{
                response(dataServer)
            }
        }
    }
    
    // Called service to get next possible list of future
    func getListNext(_ response:  @escaping( (ResponseList) -> Void), next: String){
        Constants.Connections.GET_LIST_POKEMON.requestService( url: next, listener: { (data, message) in
            if let dataServer = data?.getObject(ResponseList.self) as? ResponseList{
                response(dataServer)
            }
        }, isProgress: false)
    }
    
    
    //Called service get especific information pokemon
    func getPokemon(_ response: @escaping( (Pokemon) -> Void), url: String){
        Constants.Connections.GET_LIST_POKEMON.requestService(url: url,
                                                              listener: { (data, message) in
                                                                if let pokemon = data?.getObject(Pokemon.self) as?  Pokemon{
                                                                    response(pokemon)
                                                                }
                                                              }, isProgress: false)
    }
}
