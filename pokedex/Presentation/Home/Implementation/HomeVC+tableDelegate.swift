//
//  HomeVC+tableDelegate.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import UIKit
import Kingfisher


//Delegate of tablePokemon
extension HomeViewController: UITableViewDelegate{
    
    //Trigger to user start scrolled tableView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isScroll {
            isScroll = true
        }
    }
    //Trigger to user end scrolled tableView
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isScroll = false
        tablePokemon.reloadData()
    }
    
    //logic with user click in cellPokemon
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let pokemon = self.presenter.listFilter[indexPath.row].pokemon{
            let controller = PokemonDetailViewController()
            controller.pokemon = pokemon
            self.view.endEditing(true)
            self.pushBottom(controller)
        }
        else{
            //TODO TRAER POKEMON
        }
    }
    
    
}

extension HomeViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.listFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == self.presenter.listPreview.count - 1 {
            self.presenter.getNextList()
        }
        return generateCell(indexPath.row)
    }
    
    
    //this function generate dynamic cell for tablePokemon
    func generateCell(_ index: Int) -> UITableViewCell{
        let preview = self.presenter.listFilter[index]
        let cell = UITableViewCell()
        cell.contentView.removeAllViews()
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        let imagePokemon = UIImageView()
        cell.contentView.addSubviewWithConstraint(imagePokemon, centerY: true, leadingParent: true, constantLeading: 10,  width: 60, height: 60)
        imagePokemon.kf.setImage(with: URL(string: preview.getUrlPhoto()), placeholder: #imageLiteral(resourceName: "image_logo"))
        let labelName = UILabel()
        cell.contentView.addSubviewWithConstraint(labelName, topParent: true, constantTop: 15)
        labelName.addLeadingContraint(imagePokemon, constant: 10)
        labelName.setParameters(text: preview.name.capitalized, color: .black, font: UIFont.boldSystemFont(ofSize: 14), lines: 1)
        let labelNumber = UILabel()
        cell.contentView.addSubviewWithConstraint(labelNumber, bottomParent: true, constantBottom: -15)
        labelNumber.addLeadingContraint(imagePokemon, constant: 10)
        labelNumber.setParameters(text: preview.getNumber(), color: .lightGray, font: UIFont.systemFont(ofSize: 12), lines: 1)
        if preview.pokemon != nil {
            let pokemon = preview.pokemon!
            if pokemon.types.count > 0{
                var lastView: UIView!
                for i in 0..<pokemon.types.count{
                    let imageNature = UIImageView(image: #imageLiteral(resourceName: "image_logo"))
                    switch pokemon.types[i].type?.name {
                    case Constants.NatureTypes.BUG:
                        imageNature.image = #imageLiteral(resourceName: "Bug")
                        break
                    case Constants.NatureTypes.DARK:
                        imageNature.image = #imageLiteral(resourceName: "Dark")
                        break
                    case Constants.NatureTypes.DRAGON:
                        imageNature.image = #imageLiteral(resourceName: "Dragon")
                        break
                    case Constants.NatureTypes.ELECTRIC:
                        imageNature.image = #imageLiteral(resourceName: "Electric")
                        break
                    case Constants.NatureTypes.FAIRY:
                        imageNature.image = #imageLiteral(resourceName: "Fairy")
                        break
                    case Constants.NatureTypes.FIGHT:
                        imageNature.image = #imageLiteral(resourceName: "Fight")
                        break
                    case Constants.NatureTypes.FIRE:
                        imageNature.image = #imageLiteral(resourceName: "Fire")
                        break
                    case Constants.NatureTypes.FLYING:
                        imageNature.image = #imageLiteral(resourceName: "Flying")
                        break
                    case Constants.NatureTypes.GHOST:
                        imageNature.image = #imageLiteral(resourceName: "Ghost")
                        break
                    case Constants.NatureTypes.GRASS:
                        imageNature.image = #imageLiteral(resourceName: "Grass")
                        break
                    case Constants.NatureTypes.GROUND:
                        imageNature.image = #imageLiteral(resourceName: "Ground")
                        break
                    case Constants.NatureTypes.ICE:
                        imageNature.image = #imageLiteral(resourceName: "Ice")
                        break
                    case Constants.NatureTypes.NORMAL:
                        imageNature.image = #imageLiteral(resourceName: "Normal")
                        break
                    case Constants.NatureTypes.POISON:
                        imageNature.image = #imageLiteral(resourceName: "Poison")
                        break
                    case Constants.NatureTypes.PSYCHIC:
                        imageNature.image = #imageLiteral(resourceName: "Psychic")
                        break
                    case Constants.NatureTypes.ROCK:
                        imageNature.image = #imageLiteral(resourceName: "Rock")
                        break
                    case Constants.NatureTypes.STEEL:
                        imageNature.image = #imageLiteral(resourceName: "Steel")
                        break
                    case Constants.NatureTypes.WATER:
                        imageNature.image = #imageLiteral(resourceName: "Water")
                        break
                    
                    default:
                        break
                    }
                    
                    if i == 0 {
                        cell.contentView.addSubviewWithConstraint(imageNature, centerY: true, trailingParent: true, constantTrailing: -5, width: 40 , height: 40)
                        lastView = imageNature
                    }
                    else{
                        cell.contentView.addSubviewWithConstraint(imageNature, centerY: true, width: 40 , height: 40)
                        imageNature.addRightContraint(lastView, constant: -10)
                        lastView = imageNature
                    }
                }
            }
        }
        else{
            self.presenter.getPokemon(preview.position! - 1)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
}
