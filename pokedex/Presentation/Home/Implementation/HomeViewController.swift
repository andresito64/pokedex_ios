//
//  HomeViewController.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import UIKit

class HomeViewController: BaseViewController {
    var presenter: HomePresenter!
    var search: UISearchBar!
    var isScroll = false
    var tablePokemon: UITableView!
    private var viewHeader: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = HomePresenter(self)
        presenter.getListPreview()
        setView()
    }
    
    
    //This function set Views of screen
    private func setView(){
        setHeader()
        setTablePokemon()
        
    }
    
    //This function set tablePokemon
    
    func setTablePokemon(){
        tablePokemon = UITableView()
        self.view.addSubviewWithConstraint(tablePokemon, bottomParent: true, widhtParent: true, centerX: true)
        tablePokemon.addConstraintTop(viewHeader)
        tablePokemon.keyboardDismissMode = .onDrag
        tablePokemon.backgroundColor = .white
        tablePokemon.delegate = self
        tablePokemon.dataSource = self
    }
    
    
    //This function create header of viewController title and searchBar
    func setHeader(){
        
        //create viewGeneral
        self.view.backgroundColor = .white
        viewHeader = UIView()
        self.view.addSubviewWithConstraint(viewHeader, topParent: true, widhtParent: true, height: 150)
        
        //create background
        let imageHeader = UIImageView(image: #imageLiteral(resourceName: "Background"))
        imageHeader.contentMode = .scaleToFill
        viewHeader.addSubviewWithConstraint(imageHeader,topParent: true, widhtParent: true, heightParent: true, centerX: true)
        
        //create title
        let title = UILabel()
        viewHeader.addSubviewWithConstraint(title, topParent: true, constantTop: 40, centerX: true)
        title.setParameters(text: "Pokemon", color: .black, font: UIFont.boldSystemFont(ofSize: 18), lines: 0)
        
        //create searchbar
        search = UISearchBar()
        viewHeader.addSubviewWithConstraint(search, bottomParent: true, constantBottom: -16, widhtParent: true, widthAnchor: 0.9, centerX: true)
        search.backgroundImage = UIImage()
        search.barTintColor = .clear
        search.placeholder = "Search"
        search.delegate = self
        
        //create line gradient to bottom
        let line = UIView()
        viewHeader.addSubviewWithConstraint(line, bottomParent: true, widhtParent: true, centerX: true, height: 4)
        line.layoutIfNeeded()
        line.addGradientHorizontal(.blue, color2: .green)
    }

}
