//
//  HomePresenter.swift
//  pokedex
//
//  Created by Pablo Linares on 29/12/20.
//

import Foundation
class HomePresenter {
    
    var homeView: HomeViewDelegate!
    var listPreview: [PokemonPreview] = []
    var listFilter: [PokemonPreview] = []
    var nextUrl: String? = nil
    var homeBL: HomeBL!
    
    init(_ homeView: HomeViewDelegate) {
        self.homeView = homeView
        homeBL = HomeBL()
    }
    
    
    //this function get preview table to load pokemons
    func getListPreview(){
        homeBL.getListPokemon { (response) in
            self.nextUrl = response.next
            for i in 0..<response.results!.count{
                var preview = response.results![i]
                preview.position = i + 1
                self.listPreview.append(preview)
            }
            for preview in self.listPreview{
                self.listFilter.append(preview)
            }
            DispatchQueue.main.async {
                self.homeView.reloadTable()
            }
        }
    }
    
    
    //In case you reach the end of the list and there are new pokemons this function brings the new list
    func getNextList(){
        if let next = nextUrl{
            homeBL.getListNext({ (response) in
                self.nextUrl = response.next
                let count = self.listPreview.count
                for i in 0..<response.results!.count{
                    var preview = response.results![i]
                    preview.position = i + count
                    self.listPreview.append(preview)
                }
                self.listFilter.removeAll()
                for preview in self.listPreview{
                    self.listFilter.append(preview)
                }
                DispatchQueue.main.async {
                    self.homeView.reloadTable()
                }
            }, next: next)
        }
    }
    
    
    
    //get specific information about one pokemon
    func getPokemon(_ position: Int){
        if self.listPreview[position].pokemon != nil {
            return
        }
        homeBL.getPokemon({ (pokemon) in
            self.listPreview[position].pokemon = pokemon
            if self.listFilter.count == self.listPreview.count {
                self.listFilter[position].pokemon = pokemon
            }
            else{
                for i in 0..<self.listFilter.count where self.listFilter[i].position == position - 1{
                    self.listFilter[i].pokemon = pokemon
                }
            }
            DispatchQueue.main.async {
                self.homeView.reloadTableValidate()
            }
        }, url: self.listPreview[position].url)
    }
    
    
    //this function evaluate if searchbar is write and reload data 
    func evaluateFilter(_ searchText: String){
        self.listFilter.removeAll()
        if searchText.isEmpty {
            for preview in self.listPreview{
                self.listFilter.append(preview)
            }
        }
        else{
            self.listPreview.filter { (preview) -> Bool in
                preview.name.contains(searchText.lowercased()) || preview.getNumber().contains(searchText)
            }.forEach { (preview) in
                self.listFilter.append(preview)
            }
        }
        homeView.reloadTable()
    }
    
}
