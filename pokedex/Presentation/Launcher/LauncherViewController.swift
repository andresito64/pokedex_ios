//
//  ViewController.swift
//  pokedex
//
//  Created by Pablo Linares on 28/12/20.
//

import UIKit

class LauncherViewController: UIViewController {
    
    private var imageLogo: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.imageLogo.setSize(width: 100, height: 100)
        UIView.animate(withDuration: 1.5) {
            self.imageLogo.layoutIfNeeded()
        } completion: { (_) in
            self.navigationController?.pushViewController(HomeViewController(), animated: true)
        }


    }
    
    
    private func  setView(){
        let background = UIImageView(image: #imageLiteral(resourceName: "Background"))
        self.view.addSubviewWithConstraint(background, widhtParent: true, heightParent: true, centerX: true, centerY: true)
        background.contentMode = .scaleAspectFill
        imageLogo = UIImageView(image: #imageLiteral(resourceName: "image_logo"))
        imageLogo.frame.size = CGSize(width: 5, height: 5)
        self.view.addSubviewWithConstraint(imageLogo, centerX: true, centerY: true)
        imageLogo.contentMode = .scaleAspectFit
        
    }


}

